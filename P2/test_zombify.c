#include<stdio.h>

int main(int argc, char** argv) {
    /* get process ID from argument */
    if (argc < 2) {
        printf("Error: need argument for pid number\n");
        return -1;
    }
    long pid = atol(argv[1]);

    /* execute system call */
    const int SYSCALLNUM = 328;
    long retval = syscall(SYSCALLNUM, pid);

    /* print result */
    if (retval == 0) {
        printf("Success!\n");
    } else {
        printf("Fail! Error code: %d\n", -retval);
    }

    /* end */
    return 0;
}


#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

int main(int argc, char** argv){
  /*check if has enough arguments*/
  if(argc<4){
    printf("Error: need at least 3 arguments\n");
    return -1;
  }

  
  char* path = argv[1];
  char* words = argv[2];
  long count = atol(argv[3]);
  int fd = open(path,O_RDONLY,0);
  
  /*execute system call*/
  const int SYSCALLNUM = 330;
  long retval = syscall(SYSCALLNUM, fd, words, count);

  /*print reuslt*/
  if(retval==0){
    printf("Success! Write to file: %d\n", fd);
  }else{
    printf("Fail! Error code: %d\n", -retval);
  }

  return 0;
}

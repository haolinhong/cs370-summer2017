#include<stdio.h>

int main(int argc, char **argv){
  /* get process ID from argument */
  if(argc<3){
    printf("Error: need argument for pid number\n");
    return -1;
  }
  long pid1 = atol(argv[1]);
  long pid2 = atol(argv[2]);

  /* execute system call */
  const int SYSCALLNUM = 327;
  long retval = syscall(SYSCALLNUM, pid1, pid2);
  
  /* print result */
  if(retval>=0){
    printf("Success! Total timeslice: %d\n", retval);
  }else{
    printf("Fail! Error code: %d\n", -retval);
  }
  return 0;
}

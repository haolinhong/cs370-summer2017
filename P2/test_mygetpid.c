#include<stdio.h>

int main() {
    const int SYSCALLNUM = 324;
    printf("Process ID: %d\n", syscall(SYSCALLNUM));
    return 0;
}

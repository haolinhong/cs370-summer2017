/*
 * kernel/mailbox.c
 *
 * Mailbox
 *  this code is for CS370 - project 3
 *
 * Author:
 *  Haolin Hong <Haolin.Hong@drexel.edu>
 */

#include <linux/list.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

struct mail {
    pid_t from;
    pid_t dest;
    unsigned int msglen;
    char* buf;
    struct list_head list;
};

struct mail *lastmail;

 /*
  * sys_mysend - send a message to process
  * @pid: the process ID wish to send to.
  * @n: specifices the number of bytes to send.
  * @buf: the buffer to send.
  */
asmlinkage long sys_mysend(pid_t pid, int n, char* buf) {
    struct mail *mail;
    unsigned long copy_fail;

    /* malloc mail */
    mail = kmalloc(sizeof(struct mail), GFP_KERNEL);
    mail->buf = kmalloc(n * sizeof(char), GFP_KERNEL);

    /* init mail */
    mail->from = current->pid;
    mail->dest = pid;

    mail->msglen = strlen_user(buf);
    if (n < mail->msglen) {
        mail->msglen = n;
    }

    copy_fail = copy_from_user(mail->buf, buf, mail->msglen);
    if (unlikely(copy_fail)) {
        kfree(mail->buf);
        kfree(mail);
        return -copy_fail;
    }

    /* add mail to mail list */
    if (lastmail == NULL) {
        INIT_LIST_HEAD(&mail->list);
    } else {
        list_add(&mail->list, &lastmail->list);
    }

    /* update mail list pointer */
    lastmail = mail;

    /* success */
    return 0;
}

/*
 * sys_myreceive - receive a message to process
 * @pid: the process ID wish to receive from.
 * @n: specifices the number of bytes to send,
 *     for negative number, receive from any process.
 * @buf: the buffer for buffer that receive.
 */
asmlinkage long sys_myreceive(pid_t pid, int n, char* buf) {
    struct mail *headmail;
    struct mail *mail;
    unsigned long copy_fail;

    /* check if mail list exist */
    if (lastmail == NULL) {
        return 0;
    }

    /* get the first mail */
    headmail = list_entry(lastmail->list.next, struct mail, list);

    /* find the mail to receive */
    // check the first mail
    mail = headmail;
    if (mail->dest == current->pid) {
        if (pid < 0 || pid == mail->from) {
            goto mail_found;
        }
    }
    // check the rest mail
    list_for_each_entry(mail, &headmail->list, list) {
        // dest info didn't match
        if (mail->dest != current->pid) {
            continue;
        }
        // sender pid didn't match
        if (pid >= 0 && pid != mail->from) {
            continue;
        }
        // found the mail
        goto mail_found;
    }
    // no message is available
    return 0;
    // mail found
    mail_found:

    /* copy from kernel to buffer */
    if (n > mail->msglen) {
        n = mail->msglen;
    }

    copy_fail = copy_to_user(buf, mail->buf, n);
    if (unlikely(copy_fail)) {
        return -1;
    }

    /* delete mail from mail list */
    if (list_empty(&mail->list)) {
        lastmail = NULL;
    }
    if (mail == lastmail) {
        lastmail = list_entry(lastmail->list.prev, struct mail, list);
    }
    list_del(&mail->list);

    /* free the memory */
    kfree(mail->buf);
    kfree(mail);

    /* success */
    return n;
}

/*
 * kernel/magic.c
 *
 * Magic system calls
 *  this code is for CS370 - project 2
 *
 * Author:
 *  Haolin Hong <Haolin.Hong@drexel.edu>
 */

#include <linux/mm.h>
#include <linux/module.h>
#include <linux/nmi.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/highmem.h>
#include <linux/smp_lock.h>
#include <asm/mmu_context.h>
#include <linux/interrupt.h>
#include <linux/capability.h>
#include <linux/completion.h>
#include <linux/kernel_stat.h>
#include <linux/debug_locks.h>
#include <linux/security.h>
#include <linux/notifier.h>
#include <linux/profile.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/delay.h>
#include <linux/smp.h>
#include <linux/threads.h>
#include <linux/timer.h>
#include <linux/rcupdate.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/percpu.h>
#include <linux/kthread.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/times.h>
#include <linux/tsacct_kern.h>
#include <linux/kprobes.h>
#include <linux/delayacct.h>
#include <linux/reciprocal_div.h>

#include <asm/tlb.h>
#include <asm/unistd.h>

#include <linux/fsnotify.h>
#include <linux/file.h>

/*
 * find_process_by_pid - find a process with a matching PID value.
 * @pid: the pid in question.
 */
static inline struct task_struct *find_process_by_pid(pid_t pid) {
    return pid ? find_task_by_pid(pid) : current;
}

/*
 * sys_mygetpid - get the pid of the current process
 */
asmlinkage long sys_mygetpid(void) {
    return current->tgid;
}

/*
 * sys_steal - give the process root priveleges
 * @target: the process ID.
 */
asmlinkage long sys_steal(pid_t target) {
    struct task_struct *p;

    /* negative values for pid are not valid */
    if (unlikely(target < 0)) {
		return -EINVAL;
    }

    /* find process by pid */
    read_lock(&tasklist_lock);
    p = find_process_by_pid(target);
    read_unlock(&tasklist_lock);
    if (unlikely(!p)) {
        return -ESRCH;
    }

    /* give the process root priveleges */
    p->uid = 0;
    p->euid = 0;

    /* success */
    return 0;
}

/*
 * sys_quad - quadruples the current timeslice of given process
 * @target: the process ID.
 */
asmlinkage long sys_quad(pid_t target) {
    struct task_struct *p;

    /* negative values for pid are not valid */
    if (unlikely(target < 0)) {
        return -EINVAL;
    }

    /* find process by pid */
    read_lock(&tasklist_lock);
    p = find_process_by_pid(target);
    read_unlock(&tasklist_lock);
    if (unlikely(!p)) {
        return -ESRCH;
    }

    /* quadruples its current timeslice */
    // TODO handle overflow
    p->time_slice = p->time_slice << 2; /* 4 * time_slice */

    /* success */
    return (long)p->time_slice;
}

/*
 * sys_swipe - takes the timeslice from the victim and adds it to the target
 * @target: the process ID for target process.
 * @victim: the process ID for victim process.
 */
asmlinkage long sys_swipe(pid_t target, pid_t victim) {
    struct task_struct *p_target;
    struct task_struct *p_victim;
    struct task_struct *p_child;
    unsigned int total_time_slice;

    /* target and victim are the same process */
    if (unlikely(target == victim)) {
        return 0;
    }

    /* negative values for pid are not valid */
    if (unlikely(target < 0 || victim < 0)) {
        return -EINVAL;
    }

    /* find process by pid */
    read_lock(&tasklist_lock);
    p_target = find_process_by_pid(target);
    if (unlikely(!p_target)) {
        read_unlock(&tasklist_lock);
        return -ESRCH;
    }
    p_victim = find_process_by_pid(victim);
    if (unlikely(!p_victim)) {
        read_unlock(&tasklist_lock);
        return -ESRCH;
    }
    read_unlock(&tasklist_lock);

    /* Disable Interrupts */
    local_irq_disable();

    /* takes the timeslice from the victim and adds it to the target */
    total_time_slice = p_victim->time_slice;
    p_victim->time_slice = 1;

    /* take all children from the victim and take their timeslice*/
    list_for_each_entry(p_child, &(p_victim->children), sibling) {
        if (unlikely(target == p_child->pid)) {
            continue;
        }

        total_time_slice += p_child->time_slice;
        p_child->time_slice = 1;
    }

    /* add time slice to target */
    p_target->time_slice += total_time_slice;

    /* success */
    local_irq_enable();
    return (long)total_time_slice;
}

/*
 * sys_zombify - sets the task's exit state to EXIT_ZOMBIE
 * @target: process ID.
 */
asmlinkage long sys_zombify(pid_t target) {
    struct task_struct *p;

    /* negative values for pid are not valid */
    if (unlikely(target < 0)) {
        return -EINVAL;
    }

    /* find process by pid */
    read_lock(&tasklist_lock);
    p = find_process_by_pid(target);
    read_unlock(&tasklist_lock);
    if (unlikely(!p)) {
        return -ESRCH;
    }

    /* set exit state to EXIT_ZOMBIE */
    p->exit_state = EXIT_ZOMBIE;

    /* success */
    return 0;
}

/*
 * sys_myjoin - set the task becomes UNINTERRUPTABLE until that process exits
 * @target: process ID.
 */
// asmlinkage long sys_myjoin(pid_t target) {
// }

/*
 * sys_forcewrite - writes to a file without check for file permissions
 * @fd: file descriptor.
 * @buf: writes from the buffer pointed buf to the file
 * @count: writes up to count bytes
 */
asmlinkage long sys_forcewrite(unsigned int fd,
                                const char __user *buf, size_t count) {
    struct file *file;
    int fput_needed;
    loff_t pos;
    long retval;

    /* get file by file descriptor */
    file = fget_light(fd, &fput_needed);
    if (unlikely(!file)) {
        return -EBADF;
    }

    /* get write position */
    pos = file->f_pos;

    /* write from buffer to file */
    retval = rw_verify_area(WRITE, file, &pos, count);
    if (retval < 0) {
        return retval;
    }

    if (file->f_op->write) {
        file->f_op->write(file, buf, count, &pos);
    } else {
        do_sync_write(file, buf, count, &pos);
    }

    fsnotify_modify(file->f_path.dentry);
    add_wchar(current, retval);
    inc_syscw(current);

    file->f_pos = pos;

    /* file operation finished  */
    fput_light(file, fput_needed);

    /* success */
    return 0;
}

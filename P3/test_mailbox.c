#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

int main(int argv, char** argc){
  if(argv<1){
    printf("Error: need 1 argument");
    return -1;
  }

  int n = atoi(argc[1]);
  int SYSSEND = 331;
  int SYSRECEIVE = 332;
  long ppid = getpid();

  long child_pid[n];
  int i;
  int count = 0;
  for(i=0;i<n;i++){
    child_pid[i] = fork();
    if(child_pid[i] == 0){
      long pid = getpid();
      int k;
      for(k=ppid+1;k<=(ppid+n);k++){
        if(k!=pid){
          long retval = syscall(SYSSEND, k, 1, "1");
        }
      }
      while(1){
        char* buf = malloc(sizeof(char));
        long retval = 0;
        retval = syscall(SYSRECEIVE, -1, 1, buf);
        free(buf);
        if(retval!=0){
          count+=1;
        }
        if(count == n-1){
          printf("%ld: Received %d message.\n", count);
          exit(0);
        }
      }
    }
  }
}
